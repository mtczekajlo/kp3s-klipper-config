# Kingroon KP3S Configs

My Kingroon KP3S [Klipper](https://github.com/Klipper3d/klipper) config files.

Orca Slicer profiles moved to https://gitlab.com/mtczekajlo/kp3s-orca-slicer-profiles

## Setup

### Klipper Host

Requirements

- git
- [docker](https://docs.docker.com/engine/install/ubuntu/)

Installation

```bash
curl https://gitlab.com/mtczekajlo/kp3s-config/-/raw/master/scripts/install | bash
```

### Klipper firmware

#### Klipper SKR Pico

<https://github.com/bigtreetech/SKR-Pico>

#### Klipper KP3S MCU

<https://github.com/nehilo/Klipper-KingRoon-Printers>

```bash
sudo dnf install -y arm-none-eabi-gcc-cs arm-none-eabi-newlib
make menuconfig
```

#### Klipper GD32

<https://github.com/nehilo/Klipper-KingRoon-Printers>


![GD32 menuconfig](https://github.com/nehilo/Klipper-KingRoon-Printers/raw/main/pic/GD32.jpg)

```bash
make -j`nproc`
./scripts/update_mks_robin.py out/klipper.bin out/Robin_nano.bin
cp out/Robin_nano.bin /run/media/merolewski/disk/.
```

#### Klipper RP2040

![RP2040 menuconfig](https://canada1.discourse-cdn.com/free1/uploads/klipper/original/1X/963e419f5d95fa556540a338308bca6a0b1d1aa6.png)

```bash
make -j`nproc`
```

MPU-6050 wiring

![MPU6050 wiring](https://www.klipper3d.org/img/mpu9250-PICO-fritzing.png)

#### Input Shaping

Generating plot after resonance measuring

```bash
~/klipper/scripts/calibrate_shaper.py /tmp/calibration_data_*x*.csv -o ~/kp3s-config/klipper-config/shaper_calibrate_x.png
```

```bash
~/klipper/scripts/calibrate_shaper.py /tmp/calibration_data_*y*.csv -o ~/kp3s-config/klipper-config/shaper_calibrate_y.png
```

## Nice to know

### Links

- [**Klipper Docs**](https://www.klipper3d.org/)
- [**Klipper-KingRoon-Printers**](https://github.com/nehilo/Klipper-KingRoon-Printers)
- [**Hitori Blog Kingroon KP3S**](http://hitoriblog.com/kingroon_kp3s/docs/)
- [**Print Tuning Guide**](https://ellis3dp.com/Print-Tuning-Guide/)

### Hardware

KP3S' mainboard is basically a [MKS Robin Nano V1.3](https://github.com/makerbase-mks/MKS-Robin-Nano-V1.X/tree/master/hardware/MKS%20Robin%20Nano%20V1.3_002) clone.

Extruder and mainboard fans are by default connected to 24V directly. To make them controllable strip its connector and screw into `E1` interface or via auxiliary MOSFET modules.

To enable TMC2209's UART mode you have to desolder `30C` pull-up resistor next to two capacitors and solder the jumper below it. Repeat it for each driver module. See [this video](https://www.youtube.com/watch?v=2X_U2wZV_pw).

For M300 songs I use [this MIDI to M300 converter](https://alexyu132.github.io/midi-m300/) with `Speed multiplier` set to around `1.5` and enabled `Use "G4 after M300 (required for Duet)"` option.

![KP3S mainboard](images/KP3S_Pin_v1.3.webp)

![MKS Robin Nano mainboard](images/MKS_Robin_Nano_Wiring_v1.2.png)

![MKS Robin Nano pinout](images/MKS_Robin_Nano_Pin_v1.3.png)

![SKR_Pico_pinout](images/SKR_Pico_Pin.png)
